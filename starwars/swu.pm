package swu;

use WWW::Curl::Easy;
use URI::Escape;
use MIME::Base64 qw(encode_base64);
use File::Copy;
use JSON::XS;
use Storable;

$cache_dir = $ENV{"HOME"}."/.cache/mtg_perl/swu/";
mkdir $cache_dir unless -d $cache_dir;
$tmp_dir = "/tmp/mtg_perl";
$tmp_file = $tmp_dir."/curl.out";
mkdir $tmp_dir unless -d $tmp_dir;
open my $datafile, "+>", $tmp_file;
seek ($datafile, 0, 0);
truncate ($datafile, 0);

my $response_body;
my $curl = WWW::Curl::Easy->new;
$curl->setopt(CURLOPT_HEADER,0);
$curl->setopt(CURLOPT_FOLLOWLOCATION,1);
$curl->setopt(CURLOPT_SSL_VERIFYPEER,0);

$url_search = 'https://api.swu-db.com/cards/';

sub __curl_go {
	my $retcode = $curl->perform;
	unless ($retcode == 0) {
		print("Curl error: $retcode ".$curl->strerror($retcode)." ".$curl->errbuf."\n");
	}
}

sub print_entries {
	my $json = $_[0];
# 	my $n =  $_[1];
	$res = "";

	$hash = decode_json $json;
	if ($hash->{'total_cards'} > 0) {
		for (my $i = 0; $i < $hash->{'total_cards'}; $i++){
			$entry = $hash->{'data'}[$i];

			$traits="<traits>".join("<\/traits>\n<traits>", map(ucfirst lc,@{$entry->{'Traits'}})) ."<\/traits>\n";
			$aspects="<aspects>".join("<\/aspects>\n<aspects>", @{$entry->{'Aspects'}} )."<\/aspects>\n";
			$res .= <<ENTRY;
<entry id="">
<set>$entry->{Set}</set>
<rarity>$entry->{Rarity}</rarity>
<number>$entry->{'Number'}</number>
<title>$entry->{'Name'}</title>
<subtitle>$entry->{'Subtitle'}</subtitle>
<cost>$entry->{'Cost'}</cost>
<type>$entry->{'Type'}</type>
<arena>$entry->{'Arenas'}[0]</arena>
<traitss>
$traits
</traitss>
<aspectss>
$aspects
</aspectss>
<power>$entry->{'Power'}</power>
<hp>$entry->{'HP'}</hp>
<illustrator>$entry->{'Artist'}</illustrator>
<front-text>$entry->{'FrontText'}</front-text>
<back-text>$entry->{'BackTest'}</back-text>
<picture>$entry->{Set}_$entry->{'Number'}.png</picture>
ENTRY
			if ($entry->{'Unique'}) {
				$res .= "<uniq>true</uniq>\n"
			}
			if ($entry->{'BackArt'}) {
				$res .= "<back-picture>$entry->{Set}_$entry->{'Number'}b.png</back-picture>\n"
			}
			$res .="</entry>\n";
			$res .= '<images>';

			mkdir $cache_dir."/".$entry->{Set} unless -d $cache_dir."/".$entry->{Set};

			# FRONT PIC
			unless ( -f "$cache_dir/$entry->{Set}/$entry->{'Number'}.png" ) {
				$curl->setopt(CURLOPT_URL, $entry->{'FrontArt'});
				$curl->setopt(CURLOPT_WRITEDATA,$datafile);
				__curl_go;
				close $datafile;
				if (-s $tmp_file) {
					copy $tmp_file, "$cache_dir/$entry->{Set}/$entry->{'Number'}.png";
				} else {
					die ("Can't download image: $entry->{'FrontArt'}\n");
				}
				open $datafile, "+>", $tmp_file;
			}
			open my $pic, "$cache_dir/$entry->{Set}/$entry->{'Number'}.png";
			$res .= '<image format="PNG" id="'."$entry->{Set}_$entry->{'Number'}".'.png">';
			while (read($pic, $buf, 60*57)) {
				$res .= encode_base64($buf);
			}
			close $pic;
			$res .= '</image>';

			# BACK PIC
			if ($entry->{'BackArt'}) {
				unless ( -f "$cache_dir/$entry->{Set}/$entry->{'Number'}b.png" ) {
					$curl->setopt(CURLOPT_URL, $entry->{'BackArt'});
					$curl->setopt(CURLOPT_WRITEDATA,$datafile);
					__curl_go;
					close $datafile;
					if (-s $tmp_file) {
						copy $tmp_file, "$cache_dir/$entry->{Set}/$entry->{'Number'}b.png";
					} else {
						die ("Can't download image: $entry->{'BackArt'}\n");
					}
					open $datafile, "+>", $tmp_file;
				}
				open my $pic, "$cache_dir/$entry->{Set}/$entry->{'Number'}b.png";
				$res .= '<image format="PNG" id="'."$entry->{Set}_$entry->{'Number'}b".'.png">';
				while (read($pic, $buf, 60*57)) {
					$res .= encode_base64($buf);
				}
				close $pic;
				$res .= '</image>';
			}

			$res .= '</images>';
		}
	}

	$res =~ s/&/&amp;/g;

	print $res;
	return;
} #sub print_entries

sub header {
	return <<HEAD;
<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE tellico PUBLIC "-//Robby Stephenson/DTD Tellico V11.0//EN" "http://periapsis.org/tellico/dtd/v11/tellico.dtd"><tellico xmlns="http://periapsis.org/tellico/" syntaxVersion="11">
<collection title="My Collection" type="1">
<fields>
<field title="Name" flags="0" category="General" format="1" description="Title" type="1" name="title"/>
<field title="Subtitle" flags="0" category="General" format="1" description="Title" type="1" name="subtitle"/>
<field title="Set" flags="2" category="General" format="4" description="New Field 5" type="1" name="set"/>
<field title="Rarity" flags="2" category="General" format="4" description="New Field 6" type="1" name="rarity"/>
<field title="Card Number" flags="0" category="General" format="4" description="New Field 8" type="1" name="number"/>
<field title="Cost" flags="2" category="General" format="4" description="New Field 1" type="1" name="cost"/>
<field title="Type" flags="2" category="General" format="4" description="New Field 1" type="1" name="type"/>
<field title="Arena" flags="3" category="General" format="3" description="New Field 1" type="1" allowed="Space;Ground" name="arena"/>
<field title="Traits" flags="3" category="General" format="4" description="New Field 4" type="1" name="traits"/>
<field title="Aspects" flags="3" category="General" format="4" description="New Field 4" type="1" name="aspects"/>
<field title="Power" flags="0" category="General" format="4" description="New Field 3" type="1" name="power"/>
<field title="HP" flags="0" category="General" format="4" description="New Field 4" type="1" name="hp"/>
<field title="Unique" flags="0" category="General" format="4" description="New Field 4" type="4" name="uniq"/>
<field title="Illustrator" flags="7" category="General" format="4" description="New Field 2" type="1" name="illustrator"/>
<field title="Variant" flags="7" category="General" format="4" description="New Field 8" type="1" name="variant"/>
<field title="Card Text" flags="0" category="Card Text" format="4" description="New Field 2" type="2" name="front-text"/>
<field title="Card Text (back)" flags="0" category="Card Text" format="4" description="New Field 1" type="2" name="back-text"/>
<field title="Picture" flags="0" category="Picture" format="4" description="New Field 1" type="10" name="picture"/>
<field title="Back Picture" flags="0" category="Picture" format="4" description="New Field 1" type="10" name="back-picture"/>
</fields>
HEAD
#allowed="Vigilance;Command;Aggression;Cunning;Villainy;Heroism"
} #sub header


sub search {
	$sstr = "search?q=" . $_[0];

	$curl->setopt(CURLOPT_URL, $url_search. $sstr);
	$curl->setopt(CURLOPT_WRITEDATA,\$response_body);
	__curl_go;

	print_entries $response_body;
	return; # %hash;
} #sub search

sub build_checklist {
	$sstr = lc($_[0]);

	%lista = ();

	if ( -f "$cache_dir/sets/$sstr" ) {
		%lista = %{retrieve("$cache_dir/sets/$sstr")};
	}
	else {
		$curl->setopt(CURLOPT_URL, $url_search.$sstr);
		$curl->setopt(CURLOPT_WRITEDATA,\$response_body);
		__curl_go;

		$hash = decode_json $response_body;
		if ($hash->{'total_cards'} > 0) {
			for (my $i = 0; $i < $hash->{'total_cards'}; $i++){
				$entry = $hash->{'data'}[$i];
				if ( $entry->{VariantType} eq 'Normal') {
					$lista{ ($sstr . $entry->{Number}) } = $entry->{Name}." ".substr($entry->{Rarity},0,1);
				}
			}
			unless (-d "$cache_dir/sets" ) { mkdir "$cache_dir/sets"; }
			unless (keys( %lista) == 0) { store \%lista, "$cache_dir/sets/$sstr"; }
		}

	}

return %lista;
} #sub build_checklist

sub read_base {
	use swu_bases;

	my %lst;

	for $base (@bases) {
		my $baza = Archive::Zip->new();
		unless ( $baza->read( $base ) == AZ_OK ) {
			die 'read error';
		}
		my $set;
		foreach (split(/\n/,$baza->contents( "tellico.xml" ))) {
			if (/<set>(.+)<\/set>/) {
				$set = $1;
			}
			elsif (/<number>(\d+)<\/number>/) {
				$lst{ (lc($set) . $1) } ++;
			}
		}
	}

	return %lst;
} # sub read_base

1;
