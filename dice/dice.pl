#!/usr/bin/env perl

use Getopt::Std;
use dice;

getopt('n', \%opts);

unless ($opts{'n'}) {die "-n - search by name.\n"};

print dice::header;
print dice::search($opts{'n'});
print dice::footer;

