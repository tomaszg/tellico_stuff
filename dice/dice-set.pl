#!/usr/bin/env perl
use Getopt::Std;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );


use dice;

sub save_base {
	$dane = $_[0];

	my $baza = Archive::Zip->new();
	$baza->addString( $dane, 'tellico.xml' );
	unless ( $baza->writeToFileNamed('out.tc') == AZ_OK ) {
		die 'write error';
	}
} #sub save_base


getopts('c:n:', \%opts);

if (!$opts{'n'}) { die "-n - search by expansion\n";}

my $exp = $opts{'n'};

$out = dice::build_checklist $exp;
$out = dice::header . $out . dice::footer;
save_base $out;
