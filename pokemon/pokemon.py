#!/usr/bin/env python3
import argparse
import re
import requests
import os
import sys

from pokemontcgsdk import Card
from pokemontcgsdk import Set
from pokemontcgsdk import Type
from pokemontcgsdk import Supertype
from pokemontcgsdk import Subtype
from pokemontcgsdk import Rarity

img_dir = os.getenv("HOME") + "/.cache/mtg_perl/pokemon_cards/"

def header():
  print("""<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE tellico PUBLIC "-//Robby Stephenson/DTD Tellico V11.0//EN" "http://periapsis.org/tellico/dtd/v11/tellico.dtd"><tellico xmlns="http://periapsis.org/tellico/" syntaxVersion="11">
<collection title="My Collection" type="1">
<fields>
<field title="poke_id" flags="0" category="General" format="4" description="id" type="1" name="poke_id"/>
<field title="Name" flags="0" category="General" format="4" description="Title" type="1" name="title"/>
<field title="Color" flags="7" category="General" format="4" description="New Field 4" type="3" allowed="Colorless;Darkness;Dragon;Fairy;Fighting;Fire;Grass;Lightning;Metal;Psychic;Water" name="color"/>
<field title="Type" flags="6" category="General" format="4" description="New Field 4" type="3" allowed="Energy;Pokémon;Trainer" name="type"/>
<field title="Subtypes" flags="7" category="General" format="4" description="New Field 1" type="1" name="subtypes"/>
<field title="Evolves from" flags="6" category="General" format="4" description="Title" type="1" name="evo_from"/>
<field title="Evolves to" flags="6" category="General" format="4" description="Title" type="1" name="evo_to"/>
<field title="Abilities" flags="0" category="Card Text" format="4" description="New Field 2" type="2" name="abilities"/>
<field title="Attacks" flags="0" category="Card Text" format="4" description="New Field 2" type="2" name="attacks"/>
<field title="Rules" flags="0" category="Card Text" format="4" description="New Field 2" type="2" name="rules"/>
<field title="HP" flags="0" category="General" format="4" description="New Field 3" type="1" name="hp"/>
<field title="Set" flags="6" category="General" format="4" description="New Field 5" type="1" name="set"/>
<field title="Rarity" flags="6" category="General" format="4" description="New Field 6" type="1" name="rarity"/>
<field title="Card Number" flags="0" category="General" format="4" description="New Field 8" type="1" name="card-number"/>
<field title="Illustrator" flags="6" category="General" format="4" description="New Field 2" type="1" name="illustrator"/>
<field title="Variant" flags="7" category="General" format="4" description="New Field 8" type="1" name="variant"/>
<field title="Flavor Text" flags="0" category="Flavor Text" format="4" description="New Field 1" type="2" name="flavor-text"/>
<field title="Picture" flags="0" category="Picture" format="4" description="New Field 1" type="10" name="picture"/>
</fields>
""")

def footer():
  print('</collection></tellico>')

def list2xml(l, key):
  txt = ""
  if type(l) is not list:
    return txt
  txt += "<" + key + "s>\n"
  for i in range(len(l)):
    txt += "<" + key + ">" + l[i] + "</" + key + ">\n"
  txt += "</" + key + "s>\n"
  return txt

parser = argparse.ArgumentParser(description='Get PokemonTCG data.')
parser.add_argument('-n', '--name', dest='name', help='name to find')
parser.add_argument('-N', '--id', dest='card_id', help='card to find')
args = parser.parse_args()

if (args.name is None) == (args.card_id is None):
  sys.exit("Need to supply either -n or -N")

if args.name is not None:
  cards = Card.where(q='name:'+args.name)
else:
  cards = Card.where(q='id:'+args.card_id)
#print(cards)

header()
for x in range(len(cards)):
  # print(cards[x])
  abi_txt = ""
  rul_txt = ""
  att_txt = ""
  pic_txt = ""

  if hasattr(cards[x], "abilities") and cards[x].abilities is not None:
    for y in range(len(cards[x].abilities)):
      abi_txt += cards[x].abilities[y].name + ": " + cards[x].abilities[y].text + "(" + cards[x].abilities[y].type + ")\n"

  if hasattr(cards[x], "rules") and cards[x].rules is not None:
    for y in range(len(cards[x].rules)):
      rul_txt += cards[x].rules[y] + "\n"

  if hasattr(cards[x], "attacks") and cards[x].attacks is not None:
    for y in range(len(cards[x].attacks)):
      att_txt += str(cards[x].attacks[y].cost) + " " + cards[x].attacks[y].name
      if cards[x].attacks[y].text is not None:
        att_txt += ": " + cards[x].attacks[y].text
      if cards[x].attacks[y].damage is not None:
        att_txt += " (dmg " + cards[x].attacks[y].damage + ")\n"

  if hasattr(cards[x], "images") and cards[x].images is not None:
    url = cards[x].images.large
    ext = url[url.rfind('.'):]
    file = img_dir + cards[x].id + ext
    if not os.path.isfile(file):
      r = requests.get(url)
      with open(file, 'wb') as f:
        f.write(r.content)
  txt = f"""
<entry id="{x}">
<poke_id>{cards[x].id}</poke_id>
<title>{cards[x].name}</title>
<hp>{cards[x].hp}</hp>
<card-number>{cards[x].number}</card-number>
<set>{cards[x].set.name}</set>
<rarity>{cards[x].rarity}</rarity>
<illustrator>{cards[x].artist}</illustrator>
<abilities>{abi_txt}</abilities>
<attacks>{att_txt}</attacks>
<rules>{rul_txt}</rules>
<type>{cards[x].supertype}</type>
<picture>{file}</picture>"""
  txt += list2xml(cards[x].types, "color")
  txt += list2xml(cards[x].subtypes, "subtypes")
  if hasattr(cards[x], "evolvesFrom") and cards[x].evolvesFrom is not None:
    txt += "<evo_from>" + cards[x].evolvesFrom + "</evo_from>\n"
  if hasattr(cards[x], "evolvesTo") and cards[x].evolvesTo is not None:
    txt += "<evo_to>" + cards[x].evolvesFrom + "</evo_to>\n"
  if hasattr(cards[x], "flavorText") and cards[x].flavorText is not None:
    print("<flavor-text>" + cards[x].flavorText + "</flavor-text>\n")
  txt += "</entry>\n"
  txt = re.sub("&", "&amp;", txt)
  print(txt)
footer()
