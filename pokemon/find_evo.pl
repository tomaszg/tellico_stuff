#!/usr/bin/env perl
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use Getopt::Std;
use Data::Dumper;

@bases = ( '/home/tomaszg/spisy/pokemon.tc' );


getopts('m', \%opts);



my %lst;

for $base (@bases) {
    my $baza = Archive::Zip->new();
    unless ( $baza->read( $base ) == AZ_OK ) {
        die 'read error';
    }

    foreach (split(/<entry/,$baza->contents( "tellico.xml" ))) {
        $_ =~ /<type>Pokémon<\/type>/ || next;
#         $_ =~ /<title>(.+)<\/title>/s;
        $_ =~ /<title>(.+)<\/title>(.*<evo_from>(.+?)<\/evo_from>)*/s;
        $lst{ $1 } = $3;
#         print "$1 <== $3\n";
    }
}

# $i = 0;
# $total = 0;

my %rev_lst = reverse %lst;
my @evo2;
my @evo3;
my @miss;

foreach $k (keys %lst) {
    if ($lst{$k}) {
      if (exists $rev_lst{$k}) { next; } #skip if it will be listed elsewhere
      $from = $lst{$k};
      if (exists $lst{$from}) {
        if ($lst{$from}) {
          $from2 = $lst{$from};
          if (exists $lst{$from2}) {
            push(@evo3, "$from2 => $from => $k");
          } else {
            push(@miss, "$from2 needed by $from => $k");
          }
        } else {
        push(@evo2, "$lst{$k} => $k");
        }
      } else {
        push(@miss, "$from needed by $k");
      }
    }
}
@evo2 = sort @evo2;
@evo3 = sort @evo3;
$n = @evo2 + @evo3;

if (!$opts{'m'}) {
  print join ("\n", @evo3);
  print "\n";
  print join ("\n", @evo2);
  print "\n";
  print ($n,"\n");
} else {
  print join ("\n", @miss);
  print "\n";
}
