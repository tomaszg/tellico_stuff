#!/usr/bin/env python3

import zipfile
import xml.etree.ElementTree as ET

from pokemontcgsdk import Card
from pokemontcgsdk import Set
from pokemontcgsdk import Type
from pokemontcgsdk import Supertype
from pokemontcgsdk import Subtype
from pokemontcgsdk import Rarity
from pokemontcgsdk import RestClient

RestClient.configure('12345678-1234-1234-1234-123456789ABC')

dict = {}

with zipfile.ZipFile("/home/tomaszg/spisy/pokemon.tc","r") as myzip:
    with myzip.open("tellico.xml") as xmlfile:
        tree = ET.parse(xmlfile)
        root = tree.getroot()
        for entry in root.iter("{http://periapsis.org/tellico/}entry"):
        #for entry in root.findall("{http://periapsis.org/tellico/}entry"):
          try:
            name = entry.find('{http://periapsis.org/tellico/}title').text
            id = entry.find('{http://periapsis.org/tellico/}poke_id').text
            card = Card.find(id)
            try:
              print(id, name, card.cardmarket.prices.trendPrice)
              dict[id] = card.cardmarket.prices.trendPrice
            except:
                print(id, name)
          except:
            continue

print(*sorted(dict.items(), key=lambda item: item[1]), sep = "\n")
